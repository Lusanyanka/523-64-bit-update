import sys
from subprocess import call

if(len(sys.argv) != 3):
	print "Usage: loopcall.py <executable> <limit>"
	exit()

for i in range(1, int(sys.argv[2])):
	print "String of length: {" + str(i) + "}"
	call([sys.argv[1], i*'0'])
