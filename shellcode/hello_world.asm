section .data		; Data segment
msg		db		"Hello, world!", 0x0a	; String and newline chat

section .text		; Text segment
global _start

_start:
; SYSCALL: write(1, msg, 14)
mov eax, 4			; Write is syscall 4
mov ebx, 1			; stdout is file 1
mov ecx, msg		; ecx is the address of the string we want to print
mov edx, 14			; string is 14 bytes long
int 0x80			; make the syscall

; SYSCALL: exit(0)
mov eax, 1			; exit is syscall 1
mov ebx, 0			; exit with status 0
int 0x80			; make the syscall
